# navecocsc 的四明手题目

原牌由benito提供 (原牌新睿链接 [个人赛 第2轮 牌号 1/8](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=1N,P%3B2D,P,2H,P%3B3C,P,3H,P%3B3S,P,4D,P%3B4N,P,5S,P%3B6H,P,P,P%3B&playlog=E:QC,AC,4C,3C%3BS:QD,6D,3D,5D%3BS:4H,5H,AH,3H%3BN:AD,2D,8S,7D%3BN:KD,9D,KS,TD%3BN:6C,JD,8C,JC%3BW:2C,QH,8D,5C%3BN:2H,7H,KH,3S%3BS:TC,KC,TH,JH%3BE:&deal=QT54.AQT2.AK3.63%20J96.J73.J98542.Q%20K8.K9864.Q.AT985%20A732.5.T76.KJ742&vul=None&dealer=N&contract=6H&declarer=N&wintrick=11&score=-50&str=%E4%B8%AA%E4%BA%BA%E8%B5%9B%20%E7%AC%AC2%E8%BD%AE%20%E7%89%8C%E5%8F%B7%201/8&dealid=1019693014&pbnid=309530839))，略有修改。

## 问题

定约6♥，首攻♣Q。

<pre lang="bridge">
http://www.xinruibridge.com/deallog/DealLog.html?bidlog=1N,P%3B2D,P,2H,P%3B3C,P,3H,P%3B3S,P,4D,P%3B4N,P,5S,P%3B6H,P,P,P%3B&playlog=E:QC,AC,4C,3C%3BS:QD,6D,3D,5D%3BS:4H,5H,AH,3H%3BN:AD,2D,8S,7D%3BN:KD,9D,KS,TD%3BN:6C,JD,8C,JC%3BW:2C,QH,8D,5C%3BN:2H,7H,KH,3S%3BS:TC,KC,TH,JH%3BE:&deal=QT54.AQT2.AK3.63%20J96.J73.J98542.Q%20K8.K9864.Q.AT985%20A732.5.T76.KJ742&vul=None&dealer=N&contract=6H&declarer=N&wintrick=11&score=-50&str=%E4%B8%AA%E4%BA%BA%E8%B5%9B%20%E7%AC%AC2%E8%BD%AE%20%E7%89%8C%E5%8F%B7%201/8&dealid=1019693014&pbnid=309530839
deal=J96.J73.J98542.Q&K8.K9864.Q.AT984&A732.5.T76.KJ752&QT54.AQT2.AK3.63|ul="6♥&by South"
</pre>


## 答案

♣A吃，依次兑现♦Q、♥K、♥A、♦A、♦K，南家垫掉2张♠。东需要垫1张牌，垫♣易成只能垫♠:

<pre lang="bridge">
deal=J96.J.J98.&.986..T984&A73...KJ75&QT54.QT..6|ul="6♥&by South"
</pre>

南出♠北家将吃，出♥到Q，东受挤垫♣(如垫♠成单A易成):

<pre lang="bridge">
deal=J9..J98.&.9..T984&A7...KJ7&QT5.T..6|ul="6♥&by South"
</pre>

南出♣，东被投入，无论回♠或♣都将帮庄家做出2墩。

你答对了吗？
