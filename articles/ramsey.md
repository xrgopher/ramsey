# Ramsey的一副群组赛的实战牌例

在这个月 9 号的群组赛当中出现了一副非常有趣的牌。以下是一种可能的叫牌进程：

<pre lang="bridge">
http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,2N,P%3B3C,P,3N,P%3B6N,P,P,P%3B&playlog=E:KD,3D,4D,JD%3BE:2D,5D,7D,AD%3BN:JS,6S,5S,8S%3BN:KS,4S,7S,2S%3BN:3S,TS,AS,8H%3BS:QS,TD,4C,9S%3BS:KH,JH,4H,2H%3BS:AH,TH,9H,3H%3BS:QH,9D,8C,5H%3BS:2C,JC,QC,6C%3BN:KC,9C,6D,5C%3BN:AC,7H,6H,3C%3BN:7C,QD,8D,TC%3B&deal=82.JT8.T974.JT53%20KJ3.94.AJ.AKQ874%20T964.7532.KQ2.96%20AQ75.AKQ6.8653.2&vul=All&dealer=W&contract=6N&declarer=N&wintrick=11&score=-100&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201209%20%E7%89%8C%E5%8F%B7%204/8&dealid=995050099&pbnid=345464272
auction
</pre>

由 N 主打 6NT 定约，联手的牌如下，E 首攻 ♦K：

<pre lang="bridge">
deal|ur="群组赛1209&牌号 4/8"|cards=NS|ul=" "
</pre>

## 实战思路

让我们先从实战的角度计划一下坐庄。我们的大牌赢墩有 4♠+3♥+1♦+3♣ 共计 11 墩牌，只需要做出一墩。一个十分显然的机会是 ♣ 的平均分布，那也我们就可以简单做成。如果 ♣ 不是均分，一个比较明显的机会是持有四张 ♣ 的防守人同时持有四张 ♥，这样我们存在一个简单挤牌的机会。熟悉挤牌的同学们都知道，在实现简单挤牌的时候我们需要将输张调整到只有一个。而调整输墩只能发生在第一墩牌（否则 ♦ 会丢掉 2 墩），因此从实战的角度忍让第一墩 ♦K 是势在必行的。不妨假设 W 持有四张 ♥ 和 ♣，E 在 ♦K 吃到后转出 ♣ 试图破坏联通，我们只需要先兑现四个 ♠（N 垫一张 C）再兑现 ♦A 回到 N 以形成如下的局势：

<pre lang="bridge">
deal=.xxxx..xxx&.94.A.AK87&-&.AKQ6.865.|ur="群组赛1209&牌号 4/8"|ul=" "
</pre>
 
现在 W 已经无法垫牌，如果垫去一张 ♥ 则 S 的第四张 ♥ 可以做大，如果垫去 ♣，则 N 的 ♣ 已经建立。在这里不熟悉调整输墩的同学也可以看到在前期忍让一墩♦ 的作用，如若不然，此时 W 还可以垫掉一张闲张 ♦，挤牌不能成立。

不幸的是，在实际的分布下，由于 ♣ 和 ♥ 的长度分散在两位防家的手里，因此简单挤牌并不能成立，很多人也因此打宕了定约，我也是其中之一。四家牌如下：

<pre lang="bridge">
deal|ur="群组赛1209&牌号 4/8"
</pre>

新睿链接 [群组IMP赛 20201209 牌号 4/8](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,2N,P%3B3C,P,3N,P%3B6N,P,P,P%3B&playlog=E:KD,3D,4D,JD%3BE:2D,5D,7D,AD%3BN:JS,6S,5S,8S%3BN:KS,4S,7S,2S%3BN:3S,TS,AS,8H%3BS:QS,TD,4C,9S%3BS:KH,JH,4H,2H%3BS:AH,TH,9H,3H%3BS:QH,9D,8C,5H%3BS:2C,JC,QC,6C%3BN:KC,9C,6D,5C%3BN:AC,7H,6H,3C%3BN:7C,QD,8D,TC%3B&deal=82.JT8.T974.JT53%20KJ3.94.AJ.AKQ874%20T964.7532.KQ2.96%20AQ75.AKQ6.8653.2&vul=All&dealer=W&contract=6N&declarer=N&wintrick=11&score=-100&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201209%20%E7%89%8C%E5%8F%B7%204/8&dealid=995050099&pbnid=345464272)

## 四明手分析

不过在打牌过后我惊讶的发现这副牌是可以完成的，于是便开始从四明手的角度来思考这个问题。首先的思路是忍让第一墩，打以 ♦8 为共同威胁张，再加上 ♥ 和 ♣的双挤，不过这一思路是显然不能成立的，原因是在共同威胁花色 ♦上没有桥引。看起来完成定约需要某种比较复杂的终局打法了，我经过一段时间了思考找到了这个打法，想要自己停下来思考一下的同学可以简单提示一下，要发挥 ♥ 小号码的作用。

如果你找到了正确的打法，恭喜你！正确的打法是某种复合挤牌，首轮不能忍让，用 ♦A 拿住，之后连打四个 ♠，在第三张 ♠ 西家可以安全的垫掉一个 ♦，在第四张 ♠ 打出之前形成如下局面：

<pre lang="bridge">
deal=.JT8.T9.JT53&.94.J.AKQ874&T.7532.Q2.96&A.AKQ6.865.2|ur="群组赛1209&牌号 4/8"|ul=" "
</pre>

在 ♠A 上西家需要垫一张牌。♣ 的垫牌显然是行不通的，此时如果垫去一张 ♦，庄家可以立刻打出 ♦ 从而做大 ♦8。因此西只能垫去一张 ♥，这张牌看起来没有很大影响，实际上它使得同伴在 ♥ 上失去了保护。我们让N 垫去一张 ♣, 此时我们再兑现 3 墩 ♣ 大牌，形成这样的局势：

<pre lang="bridge">
deal=.JT.T9.JT&.94.J.A87&.7532.Q2.&.AKQ6.86.|ur="群组赛1209&牌号 4/8"
</pre>

在 ♣A 上，东家需要垫一张牌，如果垫去 ♥，则庄家的第四张 ♥ 已经做大，因此只能把 ♦ 垫成单张 Q，这时庄家只需要兑现两墩 ♥，击落西的 JT，再打出 ♦ 脱手，东家吃进之后不得不从 ♥75 向 ♥Q6 回牌，让庄家的 ♥6成为第 12 墩。

## 附加练习

这就是我今天想和大家分享的一副群组赛的牌，如果大家对这种打法感兴趣，我准备了一副练习，取自BM2000 Level5-A36。按照挤牌大全一书种的命名方法，这个例子应该是一个非常标准的双护张挤牌（Double
Guard Squeeze）感兴趣的同学不妨试试。

<pre lang="bridge">
http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,P,P&playlog=W:AS&deal=AKJT.K43.T9876.2%20432.J2.AKQ5.QJT9%20Q9876.Q98765.J.3%205.AT.432.AK87654&vul=None&dealer=W&contract=6C&declarer=S&str=bridge
deal
</pre>

面对 S 的 6♣ 定约，W 首攻 ♠A ，吃到之后续攻♠K，计划如何完成。
