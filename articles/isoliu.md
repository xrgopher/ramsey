# isoliu的牌例02 第1部分

> 作者: isoliu (文章来自2021年新书）

双方无局，叫牌过程：

<pre lang="bridge">
http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P;1N,P,3N,P;P,P&playlog=W:6H,TH,KH,5H;E:9H,AH,3H,JH;S:TD,8D,2D,6D;S:KD,QH,5D,3D;S:7D,4C,JD,4D;N:QD,9D,2C,8S;N:AD,3S,5S,6S;N:KC,6C,3C,8C;N:2S,4S,KS,AS;W:8H,9C,2H,5C;W:7H,TS,9S,7C;W:4H,TC,7S,JS;W:JC,QS,QC,AC;&deal=9743.K92.9643.Q6%20KJ5.A5.KT7.A7532%20A86.Q87643.8.J84%20QT2.JT.AQJ52.KT9&vul=None&dealer=E&contract=3N&declarer=S&wintrick=8&score=-50&str=%E5%9B%A2%E4%BD%93%E8%B5%9B%20%E7%AC%AC10%E8%BD%AE%20%E7%89%8C%E5%8F%B7%2014/16&dealid=794018966&pbnid=221536004
auction
</pre>

西家首攻♥6：

<pre lang="bridge">
deal|ul=" "|cards=NS
</pre>

第一墩，很不幸，东家打出了♥K。这个联手28点的3NT已经岌岌可危，因为庄家只有8个快速赢墩。

显然你应该先兑现♦长套。假设你忍让首攻，东家续攻♥9到你的A，西家跟3。现在你开始兑现♦长套，西家先后垫去♥Q、♣4、♠8、♠6，东家在最后一轮♦上垫去了♠3，余下的牌张是：

<pre lang="bridge">
deal=-&QT2...KT9&-&KJ5...A75|ul=" "|cards=NS|ll="NS 6/9&EW 1"
</pre>
          

现在你打算怎么打？

# 牌例02 第2部分

<pre lang="bridge">
deal=-&QT2...KT9&-&KJ5...A75|ul=" "|cards=NS|ll="NS 6/9&EW 1"
</pre>

根据东家续攻♥9，这门花色应该是6-3或7-2分布，因为如果东家的♥原本是4张或更多，那么他应该回攻原始的长四。这个信息到残局时是至关重要的。根据西家先垫♠8再垫♠6，你可以肯定他持有♠A。因此他的最后6张牌至少包括一张♠A和至少3张♥，这样一来先兑现♣K再出♣是唯一可能成功的打法。全副牌如下：

<pre lang="bridge">
deal
</pre>

实战中西家被紧逼了，他在第三轮♦上垫去了一张♣，因此庄家只要连打♣就可以完成定约。但是实战中庄家却换出了♠，错过了完成定约的机会。

在另一桌上，庄家第一墩没有忍让，他立即赢进然后兑现♦长套。西家先后垫去♥3、♣4、♠8、♠6，东家在最后一轮♦上垫去了♠3。余下的牌张是：

<pre lang="bridge">
deal=A.Q874..J8&QT2.J..KT9&974.92..Q6&KJ5.5..A75|ul=" "|ll="NS 6/9&EW 0"
</pre>

此时庄家打出♣9到暗手的A，西家机智地扔掉了♣J。这一举动可能对庄家产生了一些影响，但是此时到底是出♠还是♣可能没有非常明显的线索。实战中，庄家选择出♠，结果同样打宕了定约。续出♣可能是更合理的打法，因为如果西家原本只有5张♥，那么他多半不会在早期轻易地垫去一张。

新睿在线链接：[牌例链接1](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P;1N,P,3N,P;P,P&playlog=W:6H,TH,KH,5H;E:9H,AH,3H,JH;S:TD,8D,2D,6D;S:KD,QH,5D,3D;S:7D,4C,JD,4D;N:QD,9D,2C,8S;N:AD,3S,5S,6S;N:KC,6C,3C,8C;N:2S,4S,KS,AS;W:8H,9C,2H,5C;W:7H,TS,9S,7C;W:4H,TC,7S,JS;W:JC,QS,QC,AC;&deal=9743.K92.9643.Q6%20KJ5.A5.KT7.A7532%20A86.Q87643.8.J84%20QT2.JT.AQJ52.KT9&vul=None&dealer=E&contract=3N&declarer=S&wintrick=8&score=-50&str=%E5%9B%A2%E4%BD%93%E8%B5%9B%20%E7%AC%AC10%E8%BD%AE%20%E7%89%8C%E5%8F%B7%2014/16&dealid=794018966&pbnid=221536004)

新睿在线链接：[牌例链接2](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P;1N,P,3N,P;P,P&playlog=W:6H,TH,KH,AH;S:KD,8D,2D,6D;S:TD,3H,5D,3D;S:7D,4C,JD,4D;N:QD,9D,2C,8S;N:AD,3S,3C,6S;N:9C,6C,AC,JC;S:5S,AS,TS,4S;W:QH,JH,9H,5H;W:8H,2S,2H,5C;W:7H,TC,9S,JS;W:4H,QS,7S,7C;W:8C,KC,QC,KS;&deal=9743.K92.9643.Q6%20KJ5.A5.KT7.A7532%20A86.Q87643.8.J84%20QT2.JT.AQJ52.KT9&vul=None&dealer=E&contract=3N&declarer=S&wintrick=8&score=-50&str=%E5%9B%A2%E4%BD%93%E8%B5%9B%20%E7%AC%AC10%E8%BD%AE%20%E7%89%8C%E5%8F%B7%2014/16&dealid=793822798&pbnid=221536004)

