#!/bin/sh
set -x
cd ebooks
cp -rf ../public/book.bridge .
#mogrify -trim img/[a-d]*.png
#sed -i -e '$a\\' *.markdown
#cat 0-preface*.markdown 1-chapter[1-4]*.markdown 2-appendix*.markdown > isoliu.md

# cat *.bridge > book.bridge
sed -i 's/http:\/\/www.xinruibridge.com\/deallog/https:\/\/isoliu.gitlab.io\/deallog/g' book.bridge
pandoc -f markdown+raw_attribute -t epub book-epub.txt book.bridge --epub-cover-image book-cover.png -o book.epub

#ebook-convert isoliu.epub isoliu.mobi
cp *.epub ..
echo
echo
echo "Done!"
