#!/bin/sh

if [ $# -lt 1 ]
then
  echo "Usage: $0 <article> [folder]"
  exit 1
fi

source=$1

if [ $# -lt 2 ]
then
  folder="public" 
else
  folder=$2
fi

set -x
cd latex

echo "hello $source, $folder"
cp -rf ../$folder/*.bridge-tex .

sed -i 's/http:\/\/www.xinruibridge.com\/deallog/https:\/\/isoliu.gitlab.io\/deallog/g' *.bridge-tex

#ls -al img
multimarkdown -t latex meta.txt ${source}.bridge-tex -o article.tex

#exit
sed -i.bak 's/♦/\\textcolor{red}{$\\vardiamondsuit$}/g;s/♥/\\textcolor{red}{$\\varheartsuit$}/g;' article.tex
sed -i.bak 's/♣/$\\clubsuit$/g;s/♠/$\\spadesuit$/g;' article.tex
sed -i.bak 's/\[htbp\]/\[H]/g;' article.tex
# remove footnote, bug in mmd https://github.com/fletcher/MultiMarkdown-6/issues/163
sed -i.bak 's/\\footnote{\\href.*$//g;' article.tex #appendix.tex

# https://github.com/dvbrydon/flatex
# merge latex into one file for distribute
./flatex.py bridge.tex ${source}.tex

xelatex -interaction=nonstopmode ${source}.tex

cp ${source}.pdf  ${source}.tex ..
echo
echo
echo "Done!"
