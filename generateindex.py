#!/usr/bin/env python
import os
import re
import sys
from string import Template

dir="articles"

def href(str):
    return "<a href='%s'>%s</a>" % (str, str)

def td(str):
    return "<td class='cell'>%s</td>" % str

def row(title, topic):
    row = "<tr>"
    if topic == "book":
        row += td("book with all")
    else:
        row += td(title)
    row += td(href(topic + ".html"))
    row += td(href(topic + ".pdf"))
    if topic == "book":
        row += td(href(topic + ".epub"))
        row += td(href(topic + ".mobi"))
        row += td(href(topic + "-epub.pdf"))
    else:
        row += td("x") + td("x") + td("x")
    row += td(href(topic + ".md"))
    row += td(href(topic + ".tex"))
    row += "</tr>"
    return row

if __name__ == "__main__":
    rows = ""
    for file in sorted(os.listdir(dir)):
        if file.endswith(".md"):
            topic = file[:-len(".md")]
            rows += row(topic, topic)

    index_temp = open("index.temp.html", "r")

    src = Template(index_temp.read())
    result = src.safe_substitute(rows = rows)

    with open("index.html", "w") as index:
        index.write(result)
